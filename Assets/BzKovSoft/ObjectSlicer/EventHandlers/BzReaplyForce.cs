﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using DG.Tweening;

namespace BzKovSoft.ObjectSlicer.EventHandlers
{
    [DisallowMultipleComponent]
    class BzReaplyForce : MonoBehaviour, IBzObjectSlicedEvent
    {
        public GameObject slicedDoorCubesParent;
        public bool isSliced = false, applyForce = true;
        float randomX, randomY, randomZ;

        public void ObjectSliced(GameObject original, GameObject resultNeg, GameObject resultPos)
        {
            // we need to wait one fram to allow destroyed component to be destroyed.
            resultNeg.GetComponent<BzReaplyForce>().isSliced = true;
            resultPos.GetComponent<BzReaplyForce>().isSliced = true;
            original.GetComponent<BzReaplyForce>().isSliced = true;


            // KAPIDAKI KÜPLER İSE PARENT'I DEĞİŞTİR 
            // ÇÜNKÜ KAPI, BELİRLİ ÇOCUK KÜBÜN ALTINA DÜŞERSE GEÇİLİYOR
            if (!applyForce)
            {
                resultNeg.transform.parent = slicedDoorCubesParent.transform;
                resultPos.transform.parent = slicedDoorCubesParent.transform;

                if (resultNeg.GetComponent<MeshCollider>())
                {
                    resultNeg.GetComponent<MeshCollider>().isTrigger = true;

                }
                //resultPos.GetComponent<MeshCollider>().isTrigger = true;

                //TestObject(resultPos.transform);

                foreach (Transform t in slicedDoorCubesParent.transform)
                {
                    t.gameObject.tag = "Deformable";
                }
            }

            // OBJEYSE 2 SANİYE SONRA YOK ET
            if (original.CompareTag("Obstacle"))
            {
                StartCoroutine(DestroyAfter(resultNeg, resultPos));
            }



            StartCoroutine(NextFrame(original, resultNeg, resultPos));
        }

        private IEnumerator NextFrame(GameObject original, GameObject resultNeg, GameObject resultPos)
        {
            yield return null;

            var oRigid = original.GetComponent<Rigidbody>();
            var aRigid = resultNeg.GetComponent<Rigidbody>();
            var bRigid = resultPos.GetComponent<Rigidbody>();

            if (oRigid == null)
                yield break;

            aRigid.angularVelocity = oRigid.angularVelocity;
            bRigid.angularVelocity = oRigid.angularVelocity;
            aRigid.velocity = oRigid.velocity;
            bRigid.velocity = oRigid.velocity;

            if (applyForce)
            {
                randomX = UnityEngine.Random.Range(-5f, 5f);
                randomY = UnityEngine.Random.Range(1f, 3f);
                randomZ = UnityEngine.Random.Range(6f, 9f);

                bRigid.velocity += new Vector3(randomX, randomY, randomZ);

                randomX = UnityEngine.Random.Range(-5f, 5f);
                randomY = UnityEngine.Random.Range(1f, 3f);
                randomZ = UnityEngine.Random.Range(6f, 9f);

                aRigid.velocity += new Vector3(randomX, randomY, randomZ);
            }
            else
            {
                randomZ = UnityEngine.Random.Range(1f, 2f);

                bRigid.velocity += new Vector3(0, 0, randomZ);

                randomZ = UnityEngine.Random.Range(1f, 2f);

                aRigid.velocity += new Vector3(0, 0, randomZ);
            }

        }

        IEnumerator DestroyAfter(GameObject myObj, GameObject myObj2)
        {
            yield return new WaitForSeconds(1);

            myObj.transform.DOScale(myObj.transform.localScale * 0.1f, 2f).SetEase(Ease.Linear).OnComplete(() =>
            {
                myObj.SetActive(false);
            });

            myObj2.transform.DOScale(myObj2.transform.localScale * 0.1f, 2f).SetEase(Ease.Linear).OnComplete(() =>
            {
                myObj2.SetActive(false);
            });
        }
        //private IEnumerator NextFrame(GameObject original, GameObject resultNeg, GameObject resultPos)
        //{
        //	yield return null;

        //	var oRigid = original.GetComponent<Rigidbody>();
        //	var aRigid = resultNeg.GetComponent<Rigidbody>();
        //	var bRigid = resultPos.GetComponent<Rigidbody>();

        //	if (oRigid == null)
        //		yield break;

        //	aRigid.angularVelocity = oRigid.angularVelocity;
        //	bRigid.angularVelocity = oRigid.angularVelocity;
        //	aRigid.velocity = oRigid.velocity;
        //	bRigid.velocity = oRigid.velocity;
        //}
    }
}
