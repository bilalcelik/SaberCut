﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BzKovSoft.ObjectSlicer;
using System.Diagnostics;
using System;
using BzKovSoft.ObjectSlicer.EventHandlers;
using BzKovSoft.CharacterSlicerSamples;
using BzKovSoft;

namespace BzKovSoft.ObjectSlicerSamples
{
    /// <summary>
    /// This script will invoke slice method of IBzSliceableAsync interface if knife slices this GameObject.
    /// The script must be attached to a GameObject that have rigidbody on it and
    /// IBzSliceable implementation in one of its parent.
    /// </summary>
    [DisallowMultipleComponent]
    public class KnifeSliceableAsync : MonoBehaviour
    {
        MeshPointController meshPointController;

        bool push = true, cut = true, doorPhase = false;

        IBzSliceableAsync _sliceableAsync;

        void Start()
        {
            _sliceableAsync = GetComponentInParent<IBzSliceableAsync>();

            if (GetComponentInParent<CharacterSlicerSampleFast>())
            {
                CharacterSlicerSampleFast enemy = GetComponentInParent<CharacterSlicerSampleFast>();

                if (enemy.IsDead)
                {
                    gameObject.tag = "Untagged";
                    enemy.gameObject.tag = "Untagged";

                    foreach (Transform t in enemy.transform)
                    {
                        t.gameObject.tag = "Untagged";
                    }
                }
            }

            GameManager.Instance.DoorPhaseEvent += DoorPhase;

            meshPointController = FindObjectOfType<MeshPointController>();
        }

        void DoorPhase()
        {
            doorPhase = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            var knife = other.gameObject.GetComponent<BzKnife>();
            if (knife == null)
                return;

            if (!doorPhase)
            {
                if (knife.GetComponentInParent<SwordControl>() && knife.GetComponentInParent<SwordControl>().speed <= 0.1f && gameObject.GetComponent<BzReaplyForce>() && !gameObject.GetComponent<BzReaplyForce>().isSliced)
                {
                    //UnityEngine.Debug.Log("itt");
                    if (push)
                    {
                        cut = false;
                        GetComponent<Rigidbody>().AddTorque(transform.up * 10);
                        GetComponent<Rigidbody>().AddForce(Vector3.forward * 300);
                    }
                }
                else
                {
                    cut = true;

                    push = false;
                }
            }
            else
            {
                cut = true;

                push = false;
            }
            
        }

        void OnTriggerExit(Collider other)
        {
            var knife = other.gameObject.GetComponent<BzKnife>();
            if (knife == null)
                return;

            //if (knife.GetComponentInParent<SwordControl>().speed > 0.1f)
            //{
            //    //UnityEngine.Debug.LogError(knife.GetComponentInParent<SwordControl>().speed);
            //    if (cut)
            //    {
            //        StartCoroutine(Slice(knife));
            //        push = false;
            //    }
            //}
            if (cut)
            {
                //if (gameObject.CompareTag("Deformable"))
                //{
                //    if (meshPointController.addingPoint)
                //    {
                //        gameObject.tag = "Untagged";
                //        gameObject.GetComponent<Rigidbody>().isKinematic = false;
                //    }
                //    else
                //    {
                //        return;
                //    }
                //}

                if (!gameObject.CompareTag("Untagged"))
                {
                    StartCoroutine(Slice(knife));
                    push = false;
                }
            }

        }

        private IEnumerator Slice(BzKnife knife)
        {
            // The call from OnTriggerEnter, so some object positions are wrong.
            // We have to wait for next frame to work with correct values
            yield return null;

            Vector3 point = GetCollisionPoint(knife);
            Vector3 normal = Vector3.Cross(knife.MoveDirection, knife.BladeDirection);
            Plane plane = new Plane(normal, point);

            if (_sliceableAsync != null)
            {

                if (GetComponentInParent<Enemy>())
                {
                    Enemy enemy = GetComponentInParent<Enemy>();
                    enemy.shouldMove = false;

                    gameObject.tag = "Untagged";


                    enemy.gameObject.tag = "Untagged";

                    foreach (Transform t in enemy.transform)
                    {
                        t.gameObject.tag = "Untagged";
                    }
                }

                _sliceableAsync.Slice(plane, knife.SliceID, null);

            }

            //if (CompareTag("Deformable"))
            //{
            //    TestObject(point);
            //}

        }

        private Vector3 GetCollisionPoint(BzKnife knife)
        {
            Vector3 distToObject = transform.position - knife.Origin;
            Vector3 proj = Vector3.Project(distToObject, knife.BladeDirection);

            Vector3 collisionPoint = knife.Origin + proj;
            return collisionPoint;
        }

    }
}