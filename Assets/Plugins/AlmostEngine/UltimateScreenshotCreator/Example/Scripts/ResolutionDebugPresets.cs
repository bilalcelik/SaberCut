﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using AlmostEngine.Screenshot;

namespace AlmostEngine.Examples
{
		[InitializeOnLoad]
		public class ResolutionDebugPresets
		{

				static ResolutionDebugPresets ()
				{
						ScreenshotManager.onPresetsInit -= InitPresets;
						ScreenshotManager.onPresetsInit += InitPresets;

						ScreenshotManager.InitResolutionPresets ();
				}

				public static void InitPresets ()
				{
					ScreenshotManager.UpdateCategories ();
				}
		}
}


#endif