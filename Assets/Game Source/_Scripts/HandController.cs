﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using DG.Tweening;

public class HandController : MonoBehaviour
{
    public Transform handGroup;
    public Transform handGroupTargetPos;
    public float movementSpeed;
    public float rotationSpeed;
    public float touchDivisionValue;
    public float xValue, yValue;
    public Transform[] handGroupPositions;
    int r = 0;
    public bool inControle;
    public float v = 0.1f;

    public Vector2 xClamp, yClamp;

    // Start is called before the first frame update
    void Start()
    {
        TouchManager.Instance.onTouchBegan += TouchBegan;
        TouchManager.Instance.onTouchMoved += TouchMoved;
        TouchManager.Instance.onTouchEnded += TouchEnded;
    }

    private void TouchBegan(TouchInput touch)
    {
        inControle = true;
    }

    private void TouchMoved(TouchInput touch)
    {
        if (touch.DeltaScreenPosition.magnitude < 0.1f)
        {
            return;
        }

        xValue = touch.DeltaScreenPosition.x / touchDivisionValue;
        yValue = touch.DeltaScreenPosition.y / touchDivisionValue;

        handGroupTargetPos.position += new Vector3(xValue, yValue, 0f);
        handGroupTargetPos.position = new Vector3(Mathf.Clamp(handGroupTargetPos.position.x, xClamp.x, xClamp.y), Mathf.Clamp(handGroupTargetPos.position.y, yClamp.x, yClamp.y), 0f);
        //DUZ, SAG, SOL, UST, ALT, SAG UST, SOL UST, SAG ALT, SOL ALT
        if (xValue > v * 3f)
            r = 1;
        else if (xValue < -v * 3f)
            r = 2;
        else if (yValue > v * 3f)
            r = 3;
        else if (yValue < -v * 3f)
            r = 4;
        if (xValue > v && yValue > v)
            r = 5;
        if (xValue < -v && yValue > v)
            r = 6;
        if (xValue > v && yValue < -v)
            r = 7;
        if (xValue < -v && yValue < -v)
            r = 8;

    }

    private void TouchEnded(TouchInput touch)
    {
        inControle = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!inControle)
        {
            r = 0;
            handGroupTargetPos.position = Vector3.MoveTowards(handGroupTargetPos.position, handGroupPositions[0].position, movementSpeed * Time.deltaTime);
            handGroupTargetPos.position = new Vector3(Mathf.Clamp(handGroupTargetPos.position.x, xClamp.x, xClamp.y), Mathf.Clamp(handGroupTargetPos.position.y, yClamp.x, yClamp.y), 0f);
        }
        handGroupTargetPos.rotation = Quaternion.Lerp(handGroupTargetPos.rotation, handGroupPositions[r].rotation, rotationSpeed * Time.deltaTime);

        handGroup.position = handGroupTargetPos.position;
        handGroup.rotation = handGroupTargetPos.rotation;

        if(xValue > v || xValue < -v)
            xValue -= touchDivisionValue * 10f * Time.deltaTime;
        if (yValue > v || yValue < -v)
            yValue -= touchDivisionValue * 10f * Time.deltaTime;

    }
}
