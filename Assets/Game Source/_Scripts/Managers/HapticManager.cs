﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using UnityEngine;

public class HapticManager : MonoBehaviour
{
    public static HapticManager Instance;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    public void SetHapticsActive(bool status)
    {
        MMVibrationManager.SetHapticsActive(status);
    }

    // Light vibration on Android, and a light impact on iOS
    public void Selection()
    {
        MMVibrationManager.Haptic(HapticTypes.Selection);
    }

    // Light then heavy vibration on Android, and a success impact on iOS
    public void Success()
    {
        MMVibrationManager.Haptic(HapticTypes.Success);
    }

    // Heavy then medium vibration on Android, and a warning impact on iOS
    public void Warning()
    {
        MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    // Medium / heavy / heavy / light vibration pattern on Android, and a failure impact on iOS
    public void Failure()
    {
        MMVibrationManager.Haptic(HapticTypes.Failure);
    }

    // Light impact on iOS and a short and light vibration on Android.
    public void LightImpact()
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    // Medium impact on iOS and a medium and regular vibration on Android
    public void MediumImpact()
    {
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }
    
    // Heavy impact on iOS and a long and heavy vibration on Android
    public void HeavyImpact()
    {
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }
    
    // Short and hard impact
    public void Rigid()
    {
        MMVibrationManager.Haptic(HapticTypes.RigidImpact);
    }
    
    // Slightly longer and softer impact
    public void Soft()
    {
        MMVibrationManager.Haptic(HapticTypes.SoftImpact);
    }

    // <uses-permission android:name="android.permission.VIBRATE" />
    // For Android to support vibrations you’ll need to have the following line in your Android manifest
    public void Vibrate()
    {
        MMVibrationManager.Vibrate();
    }
    
    // This triggers a haptic of the specified intensity and sharpness (both floats between 0 and 1), in this case a mid range intensity and
    public void TransientHaptic(float intensity, float sharpness)
    {
        MMVibrationManager.TransientHaptic(intensity, sharpness);
    }

    // This triggers a continuous haptic of the specified intensity and sharpness duration,
    // intensity and sharpness both floats between 0 and 1
    // provides a fallback if continuous is not supported - none in this case, and provides this as a mono behaviour to help control the continuous haptic over time
    public void ContinuousHaptic(float intensity, float sharpness, float duration)
    {
        MMVibrationManager.ContinuousHaptic(intensity, sharpness, duration, HapticTypes.None, this);
    }

    public void ContinuousHaptic()
    {
        MMVibrationManager.StopContinuousHaptic();
    }
    
    protected virtual void UpdateContinuousHaptic(float intensity, float sharpness)
    {
        MMVibrationManager.UpdateContinuousHaptic(intensity, sharpness, true);
    }
}
