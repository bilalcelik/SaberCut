﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    public bool Vibration
    {
        get => PlayerPrefs.GetInt("Vibration", 1) == 1;
        set => PlayerPrefs.SetInt("Vibration", value ? 1 : 0);
    }

    public bool Sound
    {
        get => PlayerPrefs.GetInt("Sound", 1) == 1;
        set => PlayerPrefs.SetInt("Sound", value ? 1 : 0);
    }

    public int Level
    {
        get => PlayerPrefs.GetInt("Level", 0);
        set => PlayerPrefs.SetInt("Level", value);
    }

    public int Coin
    {
        get => PlayerPrefs.GetInt("Coin", 0);
        set => PlayerPrefs.SetInt("Coin", value);
    }
    
    public int Diamond
    {
        get => PlayerPrefs.GetInt("Diamond", 0);
        set => PlayerPrefs.SetInt("Diamond", value);
    }

}