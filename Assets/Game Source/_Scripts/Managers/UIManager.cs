﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class UIManager : MonoBehaviour
{
    #region Variables

    public static UIManager Instance;

    [Space(10)]
    [Header("Main Menu Panel")]
    public GameObject mainMenuPanel;
    public GameObject hand, handEnd;
    public TextMeshProUGUI coinTextMainMenuPanel;
    public TextMeshProUGUI diamondTextMainMenuPanel;

    [Space(10)]
    [Header("Game Play Panel")]
    public GameObject gamePlayPanel;
    public TextMeshProUGUI coinTextGamePlayPanel;
    public TextMeshProUGUI diamondTextGamePlayPanel;

    [Space(10)]
    [Header("Success Panel")]
    public GameObject successPanel;
    public GameObject[] successPanelRevealObjects;
    public TextMeshProUGUI coinTextSuccessPanel;
    public TextMeshProUGUI diamondTextSuccessPanel;

    [Space(10)]
    [Header("Fail Panel")]
    public GameObject failPanel;
    public GameObject[] failPanelRevealObjects;
    public TextMeshProUGUI coinTextFailPanel;
    public TextMeshProUGUI diamondTextFailPanel;

    [Header("Other")]
    public bool isDiamondActive;
    public GameObject[] diamonds;

    #endregion

    #region MonoBehaviour Call Backs

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    void Start()
    {
        if (!isDiamondActive)
        {
            foreach (var diamond in diamonds)
            {
                diamond.SetActive(false);
            }
        }

        coinTextMainMenuPanel.text = DataManager.Instance.Coin.ToString();
        diamondTextMainMenuPanel.text = DataManager.Instance.Diamond.ToString();

        mainMenuPanel.SetActive(true);
        gamePlayPanel.SetActive(false);
        successPanel.SetActive(false);
        failPanel.SetActive(false);

        hand.transform.DOMoveX(handEnd.transform.position.x, 1f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }

    #endregion

    #region Methods

    public void HapticButton()
    {
        DataManager.Instance.Vibration = !DataManager.Instance.Vibration;
        HapticManager.Instance.SetHapticsActive(DataManager.Instance.Vibration);

        HapticManager.Instance.Selection();
    }

    public void SoundButton()
    {
        DataManager.Instance.Sound = !DataManager.Instance.Sound;
        AudioListener.pause = DataManager.Instance.Sound;

        HapticManager.Instance.Selection();
    }

    public void SettingsButton(GameObject gameObject)
    {
        foreach (Transform child in gameObject.transform)
        {
            child.gameObject.SetActive(!child.gameObject.activeSelf);
        }

        HapticManager.Instance.Selection();
    }

    public void OnGameStarted()
    {
        HapticManager.Instance.Selection();

        GameManager.Instance.gameState = true;

        coinTextGamePlayPanel.text = DataManager.Instance.Coin.ToString();
        diamondTextGamePlayPanel.text = DataManager.Instance.Diamond.ToString();

        mainMenuPanel.SetActive(false);
        gamePlayPanel.SetActive(true);

        // OYUNCUNUN HAREKET ETMESİNİ SAĞLAR
        GameManager.Instance.swordControl.shouldMove = true;
    }

    public void CatchCurrencyTexts()
    {
        coinTextGamePlayPanel.text = DataManager.Instance.Coin.ToString();
        diamondTextGamePlayPanel.text = DataManager.Instance.Diamond.ToString();
    }

    public void OnGameFinish()
    {
        HapticManager.Instance.Success();

        SuccessPanelReveal();
    }

    public void OnGameFinishedButtonClick()
    {
        HapticManager.Instance.Selection();

        GameManager.Instance.NextLevel();
    }

    public void OnGameFailed()
    {
        HapticManager.Instance.Failure();

        Camera cam = FindObjectOfType<Camera>();

        //cam.transform.DOShakePosition(.75f, .3f, 10, 90);
        cam.transform.DOShakeRotation(2f, .5f, 10, 90);

        FailPanelReveal();
    }

    public void OnGameFailedButtonClick()
    {
        HapticManager.Instance.Selection();

        GameManager.Instance.OnGameFailed();
    }

    private void FailPanelReveal()
    {

        coinTextFailPanel.text = "+" + GameManager.Instance.coin;
        diamondTextFailPanel.text = "+" + GameManager.Instance.diamond;

        gamePlayPanel.SetActive(false);
        failPanel.SetActive(true);

        foreach (var failPanelRevealObject in failPanelRevealObjects)
        {
            failPanelRevealObject.gameObject.SetActive(true);
            failPanelRevealObject.transform.DOScale(1.5f, 1f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                failPanelRevealObject.transform.DOScale(1, 0);
            });
        }
    }

    private void SuccessPanelReveal()
    {
        coinTextSuccessPanel.text = "+" + GameManager.Instance.coin;
        diamondTextSuccessPanel.text = "+" + GameManager.Instance.diamond;

        gamePlayPanel.SetActive(false);
        successPanel.SetActive(true);

        foreach (var successPanelRevealObject in successPanelRevealObjects)
        {
            successPanelRevealObject.gameObject.SetActive(true);
            successPanelRevealObject.transform.DOScale(1.5f, 1f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                successPanelRevealObject.transform.DOScale(1, 0);
            });
        }
    }

    public void ResetUI()
    {
        mainMenuPanel.SetActive(true);
        gamePlayPanel.SetActive(false);
        successPanel.SetActive(false);
        failPanel.SetActive(false);
    }

    #endregion
}
