﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Tools;

public class GameManager : MonoBehaviour
{
    #region Variables

    public static GameManager Instance;
    [HideInInspector] public SwordControl swordControl;
    [HideInInspector] public LevelManager levelManager;

    public GameObject confetti;
    [Space]
    public GameObject player;
    public bool gameState = false;
    public int score = -1;
    public int coin = 0;
    public int diamond = 0;

    public GameStatus gameStatus;

    public List<GameObject> slicedCubes = new List<GameObject>();

    [HideInInspector] public Action DoorPhaseEvent, EndLevelEvent;//, DropSclicedCubesEvent;

    #endregion

    #region MonoBehaviour Call Backs

    private void Awake()
    {
        Application.targetFrameRate = 60;

        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;

        swordControl = FindObjectOfType<SwordControl>();
        levelManager = FindObjectOfType<LevelManager>();

        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        levelManager.player = player;

    }

    private void Start()
    {

        AudioListener.pause = DataManager.Instance.Sound;
        HapticManager.Instance.SetHapticsActive(DataManager.Instance.Vibration);

        OnGameStarted();
    }


    #endregion

    #region Methods

    private void OnGameStarted()
    {
        //TinySauce.OnGameStarted(DataManager.Instance.Level.ToString());

        gameStatus = GameStatus.game;

        UIManager.Instance.ResetUI();

        levelManager.StartLevel();

        
    }


    public void OnGameFinished()
    {
        //TinySauce.OnGameFinished(DataManager.Instance.Level.ToString(), true, score);

        gameStatus = GameStatus.endLevel;

        TouchManager.Instance.isActive = false;

        EndLevelEvent.Invoke();

        ConfettiBlast();
    }

    public void OnGameFailed()
    {

        levelManager.FailLevel();
        UIManager.Instance.ResetUI();

    }

    public void DoorPhase()
    {
        gameStatus = GameStatus.doorSlice;

        DoorPhaseEvent.Invoke();
    }

    public void NextLevel()
    {
        //gameStatus = GameStatus.endLevel;

        DataManager.Instance.Level++;


        gameStatus = GameStatus.game;

        levelManager.NextLevel();

        UIManager.Instance.ResetUI();

        TouchManager.Instance.isActive = true;

    }

    public void ConfettiBlast()
    {
        GameObject confettiDummy = Instantiate(confetti, transform);

        confettiDummy.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z + 1);


    }



    #endregion
}

public enum GameStatus
{
    game,
    doorSlice,
    endLevel,
}