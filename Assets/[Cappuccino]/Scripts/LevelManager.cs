﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject player, door;
    public GameObject[] levels, previews;

    public GameObject levelParent, sphereParent, doorParent;

    [HideInInspector] public GameObject currentLevel, nextLevel, currentDoor;

    private void Start()
    {
        GameManager.Instance.DoorPhaseEvent += DoorPhase;
    }



    public void StartLevel()
    {
        // OYUNCUNUN BULUNDUĞU VE BİR SONRAKİ LEVELİ YARATIR
        CreateLevel(true);
        CreateLevel(false);

        // OYUNCUNUN POZİSYONUNU BULUNDUĞU LEVELİN BAŞINA YOLLAR
        ResetPlayerPos();

        CreateDoor();
    }

    public void FailLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        currentLevel.SetActive(false);

        // OYUNCUNUN BULUNDUĞU LEVELDEN YARATIR
        CreateLevel(true);

        ResetPlayerPos();
        RevivePlayer();
        CreateDoorPreview();
    }

    public void NextLevel()
    {
        // BİTEN LEVELİ KAPATIR
        currentLevel.SetActive(false);
        currentDoor.SetActive(false);

        currentLevel = nextLevel;

        // OYUNCUNUN BİR SONRAKİ LEVELİNİ YARATIR
        CreateLevel(false);

        // OYUNCUNUN POZİSYONUNU BULUNDUĞU LEVELİN BAŞINA YOLLAR
        ResetPlayerPos();

        CreateDoor();

        RevivePlayer();
    }

    void CreateLevel(bool isCurrent)
    {
        // OYUNCUNUN BULUNDUĞU VEYA BİR SONRAKİ LEVELİ YARATIR
        if (isCurrent)
        {
            currentLevel = Instantiate(levels[DataManager.Instance.Level % (levels.Length)], levelParent.transform);
        }
        else
        {
            nextLevel = Instantiate(levels[(DataManager.Instance.Level + 1) % (levels.Length)], levelParent.transform);
        }
    }

    void CreateDoor()
    {
        currentDoor = Instantiate(door, doorParent.transform);

        Level curLevel = currentLevel.GetComponent<Level>();
        currentDoor.transform.position = curLevel.doorPos.position;

        CreateDoorPreview();
    }

    void CreateDoorPreview()
    {
        Level curLevel = currentLevel.GetComponent<Level>();
        int random = UnityEngine.Random.Range(0, previews.Length);

        GameObject preview = Instantiate(previews[random], currentLevel.transform);
        preview.transform.position = new Vector3(-2, 0, curLevel.doorPos.position.z - 1);
    }

    void ResetPlayerPos()
    {
        // OYUNCUNUN POZİSYONUNU BULUNDUĞU LEVELİN BAŞINA YOLLAR
        Level curLevel = currentLevel.GetComponent<Level>();
        float zPos = curLevel.startPos.position.z;
        player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, zPos);
    }

    void RevivePlayer()
    {
        player.GetComponentInChildren<Player>().died = false;
    }

    void DoorPhase()
    {
        // BÜTÜN YANIK KÜRELERİNİ SİL
        for (int i = 0; i < sphereParent.transform.childCount; i++)
        {
            Destroy(sphereParent.transform.GetChild(i).gameObject);
        }

        StartCoroutine(DestroyObstacles());
    }

    IEnumerator DestroyObstacles()
    {
        yield return new WaitForSeconds(2);

        Level curLevel = currentLevel.GetComponent<Level>();
        curLevel.obstacles.SetActive(false);
        curLevel.enemies.SetActive(false);
    }

    #region Loading Screen

    [Header("Loading Screen")]
    public GameObject loadingScreen;
    public Slider slider;

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(AsynchronousLoad(sceneIndex));
    }

    IEnumerator AsynchronousLoad(int sceneIndex)
    {
        yield return null;

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        //operation.allowSceneActivation = false;

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;
            //Debug.Log("Loading progress: " + (progress * 100) + "%");

            // Loading completed
            //if (operation.progress == 0.9f)
            //{
            //    Debug.Log("Press a key to start");
            //    if (Input.anyKeyDown)
            //        operation.allowSceneActivation = true;
            //}

            yield return null;
        }
    }

    #endregion
}
