﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Animator m_Animator;

    public float firstSpeed = 5f;
    float maxdistance, currentDis, distancePercent, speed;

    Transform target;

    public bool shouldMove = false, isSliced = false;

    private void Awake()
    {
        if (gameObject.GetComponent<Animator>() != null)
            m_Animator = gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (shouldMove)
        {
            if (target != null)
            {
                // Move our position a step closer to the target.
                //float step = speed * Time.deltaTime; // calculate distance to move
                currentDis = (target.position - transform.position).sqrMagnitude;

                //Yüzde kaç yakınlaşmış. 
                distancePercent = Mathf.InverseLerp(maxdistance, 0, currentDis);

                speed = Mathf.Lerp(firstSpeed, 0.5f, distancePercent);
                speed = speed * Time.deltaTime;

                transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.position.x, transform.position.y, transform.position.z), speed / 5);

                // Check if the position of the cube and sphere are approximately equal.
                if (Vector3.Distance(transform.position, target.position) < 1f)
                {
                    shouldMove = false;
                    StartCoroutine(DestroyAfter());
                    // Swap the position of the cylinder.
                    //target.position *= -1.0f;
                }

            }
            else
            {
                Debug.LogError("Düşman target bulamadı!");
            }

        }
    }

    bool myLock = false;
    public void RunTowardsPlayer(GameObject player)
    {
        if (!myLock)
        {
            myLock = true;

            if (m_Animator != null)
                m_Animator.SetBool("run", true);

            target = player.transform;

            maxdistance = (target.position - transform.position).sqrMagnitude;

            shouldMove = true;

        }
    }


    IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(2);

        gameObject.SetActive(false);
    }
}
