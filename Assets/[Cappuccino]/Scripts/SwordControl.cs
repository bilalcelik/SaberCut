﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using System;
using DG.Tweening;
//using Dreamteck.Splines;

public class SwordControl : MonoBehaviour
{
    public GameObject sword, planeTarget, gripTarget, myCam;

    Vector3 startLocalPos, firstStartLocalPos, targetPos;
    Quaternion startRot, parentStartRot;
    bool reset = false, isFirst = true;

    [HideInInspector] public bool shouldMove = false, doorPhase = false;
    public float playerSpeed = 1.2f;
    float currentSpeed;

    [Header("Sword Speeds")]
    [Range(0.0f, 200.0f)]
    [Tooltip("Yükseldikçe yavaşlar")]
    public float swordMovement;
    [Space]
    public float swordSpeed = 2f;
    [Tooltip("İlk yere dönüş hızı")]
    public float resetSwordSpeed = 2f;
    [Tooltip("İlk yere dönüşteki rotasyon hızı")]
    public float resetRotationSpeed = 2f;

    [Header("Sword's Max Horizontal Area")]
    public float maxYValue = 100;
    public float minYValue;
    public float maxXValue;
    public float minXValue;

    Plane plane;


    void Start()
    {
        shouldMove = false;
        currentSpeed = playerSpeed;

        TouchManager.Instance.onTouchBegan += TouchBegan;
        TouchManager.Instance.onTouchMoved += TouchMoved;
        TouchManager.Instance.onTouchEnded += TouchEnded;

        GameManager.Instance.DoorPhaseEvent += DoorPhase;
        GameManager.Instance.EndLevelEvent += EndLevel;

    }

    void DoorPhase()
    {
        //startLocalPos = transform.localPosition;
        transform.parent.rotation = parentStartRot;
        //splineFollower.follow = false;

        //myCamLocalPos = myCam.transform.localPosition;

        doorPhase = true;

        plane = new Plane(Vector3.back, planeTarget.transform.position);

        //startLocalPos = new Vector3(startLocalPos.x, startLocalPos.y, startLocalPos.z - 1f);

        myCam.transform.DOLocalMoveZ(myCam.transform.localPosition.z - 1f, 1f).OnComplete(() =>
        {
            transform.DORotate(new Vector3(0, 180, 0), 1f).OnComplete(() => TouchManager.Instance.isActive = true);
        });

        reset = true;

    }

    void EndLevel()
    {
        doorPhase = false;

        transform.localPosition = firstStartLocalPos;
        startLocalPos = firstStartLocalPos;

        transform.rotation = startRot;
        //startLocalPos = new Vector3(startLocalPos.x, startLocalPos.y, startLocalPos.z + 1f);
        //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);

        transform.DORotateQuaternion(startRot, 1f);

        myCam.transform.DOLocalMoveZ(myCam.transform.localPosition.z + 1f, 1f);

        reset = true;
    }

    bool firstSwordStab = false;
    private void TouchBegan(TouchInput touch)
    {
        reset = false;
        if (isFirst)
        {
            //startPos = GetPoint();
            //startPos = transform.parent.position;
            parentStartRot = transform.parent.rotation;

            startLocalPos = transform.localPosition;
            firstStartLocalPos = transform.localPosition;
            startRot = transform.rotation;
            isFirst = false;

            //splineFollower.follow = true;
        }

        if (doorPhase)
        {
            Vector3 doorPoint = GetDoorPoint();
            Vector3 swordPoint = new Vector3(doorPoint.x, doorPoint.y + 0.5f, doorPoint.z - 1f);

            transform.DOMove(swordPoint, 20f).SetSpeedBased().SetEase(Ease.Linear).OnComplete(() =>
            {
                firstSwordStab = true;
            });
        }

    }

    private void TouchEnded(TouchInput touch)
    {
        reset = true;
        firstSwordStab = false;

    }

    float xValue, yValue;
    private void TouchMoved(TouchInput touch)
    {
        if (!doorPhase)
        {
            xValue = touch.DeltaScreenPosition.x / swordMovement;
            yValue = touch.DeltaScreenPosition.y / swordMovement;

            if (touch.DeltaScreenPosition.y > maxYValue)
            {
                yValue = maxYValue / swordMovement;
            }
            if (touch.DeltaScreenPosition.y < minYValue)
            {
                yValue = minYValue / swordMovement;
            }
            if (touch.DeltaScreenPosition.x > maxXValue)
            {
                xValue = maxXValue / swordMovement;
            }
            if (touch.DeltaScreenPosition.x < minXValue)
            {
                xValue = minXValue / swordMovement;
            }

            float step = swordSpeed * Time.deltaTime;

            targetPos = new Vector3(xValue, yValue, transform.position.z);

            transform.position = Vector3.MoveTowards(transform.position, targetPos, step);

            transform.LookAt(gripTarget.transform);
        }
        else
        {
            Vector3 doorPoint = GetDoorPoint();
            Vector3 swordPoint = new Vector3(doorPoint.x, doorPoint.y + 0.5f, doorPoint.z - 1f);

            //transform.position = Vector3.Lerp(transform.position, new Vector3(swordPoint.x, swordPoint.y, transform.position.z), 2 * Time.deltaTime);

            if (firstSwordStab)
            {
                transform.position = Vector3.Lerp(transform.position, swordPoint, 2 * Time.deltaTime);
            }
        }


    }
    //public Quaternion FindPerpendicularAngle(Vector3 pos1, Vector3 pos2)
    //{
    //    Quaternion temp = Quaternion.LookRotation(pos2 - pos1); 
    //    return new Quaternion(temp.x, temp.y + 90f, temp.z, temp.w);
    //}

    Vector3 GetDoorPoint()
    {
        float distance;

        //Vector3 vector = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + 3f);

        //Plane plane = new Plane(Vector3.back, planeTarget.transform.position);
        //Ray ray = Camera.main.ScreenPointToRay(MousePos());
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (plane.Raycast(ray, out distance))
        {
            return ray.GetPoint(distance);
        }

        return Vector3.zero;
    }

    bool isSlowed = false;
    float slowTimer = 1f, currentSlowTimer;
    public void SlowPlayer()
    {
        if (!isSlowed)
        {
            isSlowed = true;
            DOTween.To(() => currentSpeed, x => currentSpeed = x, .25f, .5f);
        }
        else
        {
            currentSlowTimer = slowTimer;
        }
    }

    [HideInInspector] public float speed = 0;
    Vector2 lastPosition = Vector2.zero;
    Vector2 transformPos;
    void FixedUpdate()
    {
        //Debug.Log(speed);
        transformPos = new Vector2(transform.position.x, transform.position.y);
        speed = (transformPos - lastPosition).magnitude;
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (shouldMove)
        {
            transform.parent.position += (Vector3.forward / 10) * currentSpeed;
        }

        if (reset)
        {
            float posStep = resetSwordSpeed * Time.deltaTime * 2;
            float rotStep = resetRotationSpeed * Time.deltaTime * 10;

            //Vector3 newDirection = Vector3.RotateTowards(transform.forward, startRot.eulerAngles, step, 0f);
            //transform.rotation = Quaternion.LookRotation(newDirection);

            if (!doorPhase)
            {
                //transform.rotation = Quaternion.RotateTowards(transform.rotation, startRot, rotStep);
                //transform.localPosition = Vector3.MoveTowards(transform.localPosition, startLocalPos, posStep);
            }
            else
            {
                //transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(transform.localPosition.x, transform.localPosition.y, startLocalPos.z), 2 * posStep);
                //transform.localPosition = Vector3.MoveTowards(transform.localPosition, startLocalPos, posStep);

                transform.localPosition = Vector3.Lerp(transform.localPosition, startLocalPos, 2 * Time.deltaTime);

            }

        }

        if (isSlowed)
        {
            currentSlowTimer -= Time.deltaTime;
            if (currentSlowTimer <= 0)
            {
                isSlowed = false;
                currentSlowTimer = slowTimer;

                DOTween.To(() => currentSpeed, x => currentSpeed = x, playerSpeed, .5f);

            }
        }

    }


}
