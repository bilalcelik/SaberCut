﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;
using System.Linq;
using BzKovSoft.ObjectSlicer.EventHandlers;

public class Deformer : MonoBehaviour
{

    private MeshCollider mc;

    public GameObject brokenDoorParent;
    public Transform lowestTransform;
    public List<GameObject> brokenDoorPart = new List<GameObject>();

    private void Awake()
    {
        mc = GetComponent<MeshCollider>();
    }

    private void Start()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.5f);
        transform.DOMoveZ(transform.position.z + 2f, 0.5f).OnComplete(() =>
        {
            mc.enabled = false;

            FindLowestCube();
        });
    }

    void FindLowestCube()
    {
        // EN ALTTAKİ KÜP (KÜRELER SEÇİLMİYOR)
        Transform topLowest = brokenDoorParent.transform.Cast<Transform>().Where(t => t.gameObject.tag != "Sphere").OrderBy(t => t.position.y).First();

        if (topLowest != null)
        {
            lowestTransform = topLowest;


            for (int i = 0; i < brokenDoorPart.Count; i++)
            {
                if (brokenDoorPart[i].transform != lowestTransform)
                {
                    brokenDoorPart[i].transform.parent = lowestTransform;
                }
            }
        }
        else
        {
            Debug.LogError("En Alt Küp Bulunamadı");
        }

        // DÜŞECEK PARÇAYI VE ÇOCUKLARININ TAGINI DEĞİŞTİRİYORUZ Kİ SONRAKİ KESİMDE İÇİNE ALMASIN
       
        foreach (Transform t in lowestTransform)
        {
            if (t.gameObject.tag != "Sphere")
            {
                t.gameObject.tag = "Untagged";
            }
            else
            {
                t.gameObject.tag = "Slicer";
            }
        }
        lowestTransform.tag = "Untagged";


        if (lowestTransform != null)
        {
            FallBrokenPart();
        }
        else
        {
            Debug.Log("KESEMEDİN");
        }

    }

    void FallBrokenPart()
    {
        //-1.65

        Vector3 rotateVector = new Vector3(0, -90, -90);

        lowestTransform.DOMoveZ(lowestTransform.position.z + 10f, 1f).SetEase(Ease.Linear).SetSpeedBased();

        Sequence mySequence2 = DOTween.Sequence();
        mySequence2.Append(lowestTransform.DOMoveY(-1.65f, 1f).SetEase(Ease.InSine).SetSpeedBased().OnComplete(() =>
        {
            lowestTransform.DOKill();
        }))
          .Insert(0, lowestTransform.DORotate(rotateVector, mySequence2.Duration()).SetEase(Ease.InQuad));

        CheckSuccess();
    }

    private void CheckSuccess()
    {
        GameObject currentDoor = GameManager.Instance.levelManager.currentDoor;//.levels[DataManager.Instance.Level % GameManager.Instance.levels.Length];

        // EĞER KAPIDAKI KÜPLER BELİRLİ SAYININ ALTINA DÜŞTÜYSE DİĞER LEVELE GEÇ
        if (currentDoor.transform.GetChild(1).transform.childCount < 475 && GameManager.Instance.gameStatus == GameStatus.doorSlice)
        {
            GameManager.Instance.OnGameFinished();

            // OYUNCUYU DELİKTEN GEÇİRİP UI ÇIKARTIR
            Transform player = GameManager.Instance.swordControl.transform.parent;
            player.DOMoveZ(player.position.z + 10f, 3f).SetEase(Ease.Linear).OnComplete(() =>
            {
                UIManager.Instance.OnGameFinish();
            });
        }
    }
    private void OnCollisionEnter(Collision collision)
    {

        //Eğer kapıdaki küplere değdiyse küpleri bir obje içine alıp rotate ettirip düşürecek bu kısım
        if (collision.gameObject.tag.Equals("Deformable"))
        {
            collision.collider.isTrigger = false;

            collision.gameObject.tag = "Untagged";

            brokenDoorPart.Add(collision.gameObject);
            collision.transform.parent = brokenDoorParent.transform;

        }

        if (collision.gameObject.tag.Equals("Sphere"))
        {
            collision.collider.isTrigger = false;

            brokenDoorPart.Add(collision.gameObject);
            collision.transform.parent = brokenDoorParent.transform;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //Eğer kapıdaki küplere değdiyse küpleri bir obje içine alıp rotate ettirip düşürecek bu kısım
        if (other.tag.Equals("Deformable"))
        {
            other.isTrigger = false;

            other.tag = "Untagged";

            brokenDoorPart.Add(other.gameObject);
            other.transform.parent = brokenDoorParent.transform;

        }

        if (other.tag.Equals("Sphere"))
        {
            other.isTrigger = false;

            brokenDoorPart.Add(other.gameObject);
            other.transform.parent = brokenDoorParent.transform;
        }
    }
}