﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

public class MeshPointController : MonoBehaviour
{
    [SerializeField] private List<MeshPoint> meshPoints;

    [SerializeField] private float lifeTime, distBetweenPoints;

    private MeshCreator meshCreator;

    private int pointCount;

    //meshCountDown optimizasyon için. Ard arda mesh oluşturmasın, her mesh arasında süre kalsın
    private float lastMesh, meshCountDown = 1.5f;

    public bool addingPoint = false;

    private void Awake()
    {
        meshPoints = new List<MeshPoint>();
        meshCreator = FindObjectOfType<MeshCreator>();
    }

    private void Start()
    {
        meshCountDown = 1.5f;

        TouchManager.Instance.onTouchMoved += TouchMoved;
        GameManager.Instance.EndLevelEvent += EndLevel;
    }

    private void EndLevel()
    {
        meshPoints = new List<MeshPoint>();
        pointCount = 0;
    }

    private void TouchMoved(TouchInput touch)
    {
        if (GameManager.Instance.gameStatus == GameStatus.doorSlice && touch.DeltaScreenPosition.magnitude > 0)
        {
            OnSwordMove();
        }
    }


    public void OnSwordMove()
    {
        if (pointCount == 0)
        {
            AddPoint();
        }
        else
        {
            if ((transform.position - meshPoints[pointCount - 1].point).sqrMagnitude > distBetweenPoints)
            {
                AddPoint();
            }
        }
    }

    void AddPoint()
    {
        Vector3 tempPoint = transform.position;

        MeshPoint temp = new MeshPoint(tempPoint, Time.time);
        meshPoints.Add(temp);
        pointCount++;

        //TestObject(tempPoint);

        Control(tempPoint);
    }

    void TestObject(Vector3 point)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //sphere.GetComponent<Rigidbody>().isKinematic = true;
        //sphere.GetComponent<Collider>().enabled = false;

        sphere.transform.position = new Vector3(point.x, point.y, point.z - 0.75f);
        sphere.transform.localScale = Vector3.one * 0.2f;
        sphere.transform.parent = GameManager.Instance.levelManager.sphereParent.transform;
    }
    void Control(Vector3 point)
    {
        //Kesişim var mı diye kontrol eder
        for (int i = pointCount - 2; i >= 0; i--)
        {
            if ((meshPoints[i].point - point).sqrMagnitude < distBetweenPoints)
            {
                GetPoints(meshPoints[i].point);
                return;
            }
        }
    }

    void GetPoints(Vector3 point)
    {
        //TODO Kılıç durur

        //Optimizasyon
        if (Time.time < meshCountDown + lastMesh)
        {
            return;
        }

        int targetIndex = IndexOf(point);

        List<Vector3> meshPointsVector3 = new List<Vector3>();


        for (int i = pointCount - 1; i >= targetIndex; i--)
        {
            Vector3 temp = meshPoints[i].point;

            //Tüm noktalar ayno hizada olunca mesh kötü oluşuyor. Bu if sayesinde noktaların yarısını küçük bi birim hareket ettiriyorum
            if (i % 2 == 0)
            {
                temp.z += 0.1f;
            }

            //Kapalı şeklin noktalarında küre oluşturur
            //if (i != targetIndex)
            //{
            //    TestObject(temp);
            //}

            meshPointsVector3.Add(temp);
        }

        lastMesh = Time.time;
        pointCount--;
        meshPoints.RemoveAt(targetIndex);
        meshCreator.CreateMesh(meshPointsVector3);
    }

    int IndexOf(Vector3 point)
    {
        for (int i = 0; i < meshPoints.Count; i++)
        {
            if (meshPoints[i].point == point)
            {
                return i;
            }
        }

        return 0;
    }
}

[Serializable]
class MeshPoint
{
    public Vector3 point;
    public float time;

    public MeshPoint(Vector3 point, float time)
    {
        this.time = time;
        this.point = point;
    }
}