﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyRadar : MonoBehaviour
{

    public GameObject playerBody;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponentInParent<Enemy>().RunTowardsPlayer(playerBody);

            if(other.GetComponentInParent<Animator>() != null)
            {
                Animator animator = other.GetComponentInParent<Animator>();

                DOTween.To(() => animator.speed, x => animator.speed = x, .5f, .5f);
            }
            
        }
    }
}
