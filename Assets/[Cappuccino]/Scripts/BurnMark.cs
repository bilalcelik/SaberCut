﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

public class BurnMark : MonoBehaviour
{
    bool doorPhase = false, slicing = false, touchEnd = true;

    Vector3 hitPos;

    float timer = 0.15f, swordMoveSpeed;

    public Material burnMaterial;

    void Start()
    {

        TouchManager.Instance.onTouchBegan += TouchBegan;
        TouchManager.Instance.onTouchMoved += TouchMoved;
        TouchManager.Instance.onTouchEnded += TouchEnded;

        GameManager.Instance.DoorPhaseEvent += DoorPhase;
        GameManager.Instance.EndLevelEvent += EndLevel;

    }

    private void Update()
    {
        if (slicing)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                CreateBurnMark(hitPos);

                if (swordMoveSpeed > 1)
                    timer = 0.05f / swordMoveSpeed;
                else
                    timer = 0.05f;
            }
        }
    }

    private void DoorPhase()
    {
        doorPhase = true;
        touchEnd = true;
    }

    private void EndLevel()
    {
        doorPhase = false;

    }

    private void TouchBegan(TouchInput touch)
    {
        touchEnd = false;
    }
    private void TouchEnded(TouchInput touch)
    {

        touchEnd = true;
        slicing = false;
        timer = 0.15f;
    }

    private void TouchMoved(TouchInput touch)
    {
        swordMoveSpeed = touch.DeltaScreenByMove.magnitude / 5;

        if (doorPhase && !touchEnd)
        {
            Vector3 fwd = transform.TransformDirection(Vector3.forward) * 10;

            Debug.DrawRay(transform.position, fwd, Color.green);

            RaycastHit hit;

            if (Physics.Raycast(transform.position, fwd, out hit))
            {
                if (hit.transform.tag == "Deformable")
                {
                    slicing = true;

                    hitPos = hit.point;

                }
                else
                {
                    Debug.Log(hit.transform.tag);
                    slicing = false;

                }
            }
        }
    }

    void CreateBurnMark(Vector3 point)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        sphere.GetComponent<MeshRenderer>().material = burnMaterial;

        sphere.gameObject.layer = 2;

        sphere.GetComponent<Collider>().isTrigger = true;
        sphere.GetComponent<SphereCollider>().radius = 2f;


        sphere.tag = "Sphere";
        sphere.AddComponent<Rigidbody>();
        sphere.GetComponent<Rigidbody>().isKinematic = true;

        sphere.transform.position = point;
        sphere.transform.localScale = Vector3.one * 0.15f;

        sphere.transform.parent = GameManager.Instance.levelManager.sphereParent.transform;

       
    }



}
