﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BzKovSoft.ObjectSlicer.EventHandlers;
using BzKovSoft.CharacterSlicerSamples;
using Tools;

public class Player : MonoBehaviour
{
    public bool died = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && !died)
        {
            if (other.GetComponentInParent<CharacterSlicerSampleFast>() && !other.GetComponentInParent<CharacterSlicerSampleFast>().IsDead)
            {
                died = true;

                GameManager.Instance.swordControl.shouldMove = false;

                UIManager.Instance.OnGameFailed();
            }

        }
        else if (other.tag == "Obstacle")
        {
            if (other.GetComponent<BzReaplyForce>() && !other.GetComponent<BzReaplyForce>().isSliced)
            {
                GameManager.Instance.swordControl.shouldMove = false;
                UIManager.Instance.OnGameFailed();
            }
        }
        
        if (other.tag == "EndLevel")
        {
            GameManager.Instance.swordControl.shouldMove = false;
            TouchManager.Instance.isActive = false;

            GameManager.Instance.DoorPhase();
        }
    }
}
