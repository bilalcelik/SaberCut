using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRadar : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {

            if (other.GetComponentInParent<Animator>() != null)
            {
                Animator animator = other.GetComponentInParent<Animator>();

                if (animator != null)
                    animator.SetBool("attack", true);

                GameManager.Instance.swordControl.SlowPlayer();
            }

        }
    }
}
