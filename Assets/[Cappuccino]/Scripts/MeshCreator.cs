﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MeshCreator : MonoBehaviour
{
    //public GameObject brokenPart;
    
    public void CreateMesh(List<Vector3> points)
    {

        #region Mesh Oluşturma
        Poly2Mesh.Polygon polygon = new Poly2Mesh.Polygon();
        polygon.outside = points;

        polygon.CalcPlaneNormal();
        
        GameObject deformer = Poly2Mesh.CreateGameObject(polygon);
        deformer.GetComponent<MeshRenderer>().enabled = false;
        MeshCollider collider = deformer.AddComponent<MeshCollider>();
        Rigidbody rb = deformer.AddComponent<Rigidbody>();

        // MESHİ BİRAZ KÜÇÜLTÜYORUZ Kİ KESİLEN KÜPLERİN İÇ TARAFINI ALSIN SADECE
        deformer.transform.localScale = new Vector3(deformer.transform.localScale.x * 0.96f, deformer.transform.localScale.y, deformer.transform.localScale.z);

        rb.isKinematic = true;
        //collider.convex = true;
        //collider.isTrigger = true;
        #endregion

        #region Mesh Parent'ı Bulma

        GameObject brokenDoorParent = new GameObject("BrokenDoorParent");

        GameObject currentLevel = GameManager.Instance.levelManager.currentLevel;//.levels[DataManager.Instance.Level % GameManager.Instance.levels.Length];

        brokenDoorParent.transform.parent = currentLevel.transform;
        brokenDoorParent.transform.position = GameManager.Instance.swordControl.transform.position;
        #endregion

        
        //Mesh'e deformerı veriyorum
        deformer.AddComponent<Deformer>();

        deformer.GetComponent<Deformer>().brokenDoorParent = brokenDoorParent;
    }
}
